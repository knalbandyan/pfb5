console.log("Programming Fundamentals for Beginners");

//Գտնել երեք թվերից մեծագույնը։

var max;
var a = 10, b = 20, c = 3;

max = a;

if(b > max) {
	max = b;
}

if(c > max) {
	max = c;
}

console.log(max);

/*Ներմուծված տարիքից կախված արտածել, 
արդյո՞ք դիմորդը իրավունք ունի վարորդական իրավունքի քննություն հանձնելու, թե՞ ոչ։
*/
var age = 16;
var isExamPassed = true; //false

if(age >= 18)
{
	console.log("Allowed");

	if(isExamPassed) 
	{
		console.log("Get License");
	}
	else 
	{
		console.log("Sorry..");
	}
}
else 
{
	console.log("NO");
}

/*
Արտածել համապատասխանաբար ցուրտ է (t <= 18 °C), 
հաճելի է (18 °C < t < 24 °C), 
շոգ է (t >= 24 °C) կախված սենյակի ջերմաստիճանից:
*/

var t = 5;

if(t <= 18) {
	console.log("ցուրտ է");
}

if(t >= 24) { //t > 24 || t == 24
	console.log("շոգ է");
}

if(t > 18 && t < 24) { 
	console.log("հաճելի է");
}

/* ==== While Loop ==== */
/* ==================== */
var x = 1;

if(x < 5) {
	console.log(x);
}

console.log("loop start");

while(x < 5) {
	console.log(x);
	x = x + 1;
}

console.log("loop end");