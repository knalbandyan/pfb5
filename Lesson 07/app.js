'use strict';
console.log("Programming Fundamentals for Beginners");

/*Գրել ֆունկցիա, որը կվերադարձնի true, եթե արգումենտում փոխանցված ամբողջ թիվը զույգ է և false՝ եթե կենտ է։*/
var IsEven = function(value) {
	var result = (value % 2 == 0);
	return result;
	//return value % 2 == 0;
}

console.log(IsEven(5));
console.log(IsEven(6));

/*Գրել ֆունկցիա, որը կվերադարձնի արգումենտում փոխանցված թվային զանգվածի ամենամեծ (ամենափոքր) էլեմենտի արժեքը։*/


/*Գրել ֆունկցիա, որը կվերադարձնի արգումենտում փոխանցված թվային զանգվածի ամենամեծ (ամենափոքր) էլեմենտի ինդեքսը։*/


/*Գրել ֆունկցիա, որը կվերադարձնի արգումենտում փոխանցված թվային զանգվածի էլեմենտների գումարը։*/


/*Գրել ֆունկցիա, որը կվերադարձնի արգումենտում փոխանցված թվային զանգվածի էլեմենտների արտադրյալը։*/


/*Գրել ֆունկցիա, որը կարտածի արգումենտում փոխանցված թվային զանգվածի էլեմենտների արժեքները։*/

var a = [1, 2, 3, 4, 5];
PrintArray(a, "a");


/* For Loop */
var n = 5;

var i = 0;

while(i < n) {
	i += 1;
}

for(var j = 0; j < n; j += 1) {

}

//increment
// a = a + 1; a += 1;
var a = 1;
console.log(a);
console.log(a++); //post increment { console.log(b); b += 1; }
console.log(a);

var b = 1;
console.log(b);
console.log(++b); //pre increment { b += 1; console.log(b); }
console.log(b);

var k = a++; // { var k = a; a += 1; }
var k = ++a; // { a += 1; var k = a; }

for(var p = 0; p < n; p++) {

}